"use strict";
var fs = require('fs');
var process = require('process');
var workingDirectory = process.cwd().slice(2);
var XLSX = require('xlsx');
var workbook = XLSX.readFile(process.argv[2]);
var sheets = workbook.Sheets;
var htmlFile = '';
// change these to process more sheets/columns
var numCols = 3;
var numSheets = 1;
// Help for searching: type text and press <enter>. Repeat <enter> to cycle through results
var placeholderText = 'zoektekst en &lt;enter> (opnieuw &lt;enter> voor volgende)';
var tooltipText = 'Type een zoektekst, daarna &lt;enter> om te zoeken. Opnieuw &lt;enter> typen zoekt het volgende resultaat. Werkt niet in Edge. Met Ctrl-F kan ook gezocht worden.';

// Check to make sure user provides argument for command line
if (typeof process.argv[2] === 'undefined') {
	console.log('\n' + 'Error:' + '\n' + 'You must enter the excel file you wish to build tables from as an argument' + '\n' + 'i.e., node toTable.js resolutions.xlsx');
	return;
} else {
	// Check that the file is the correct type
	if (process.argv[2].slice(-4) !== 'xlsx') {
		console.log('\n' + 'This program will only convert xlsx files' + '\n' + 'Please enter correct file type');
		return;
	} else {
		// Create the HTML file name to write the table to
		var fileName = process.argv[2];
		var newFileName = fileName.slice(0, -5) + '-nl.html';
	}
}

htmlFile += '<!DOCTYPE html>\n';
htmlFile += '<html><head><meta charset="UTF-8"/></head><body><div class="xlsx">\n';

htmlFile += '<style>.odd {background-color: #eeeeee}</style>';

function getPosition(string, subString, index) {
   return string.split(subString, index).join(subString).length;
}
var shn = 1;
// Iterate through each worksheet in the workbook
for (var sheet in sheets) {
	
	// Start building a new table if the worksheet has entries
	if (typeof sheet !== 'undefined') {
		htmlFile += '<div style="margin: 5px; text-align: center">mei Ctrl-F kist sykje yn de pagina</div>';
		htmlFile += '<table>\n';		
		// Iterate over each cell value on the sheet
		var closed = true;
		var row = 0;
		var celln = 0;
		for (var cell in sheets[sheet]) {
			if (cell.slice(0, 1) === 'A') {
			    celln = 0;
				if (!closed) htmlFile += '</tr>\n';
				closed=false;
				htmlFile += '<tr' + ((++row % 2 == 0) ? '' : ' class="odd"') + '>';
			}
			if (++celln > numCols) continue;
			// Protect against undefined values
			if (typeof sheets[sheet][cell].w !== 'undefined') {
                htmlFile += '<td>' + sheets[sheet][cell].w.replace('&', '&amp;').replace('<', '&lt;') + '</td>';
			}
			if (celln===numCols) {htmlFile += '</tr>\n'; closed=true;}
		}
		if (!closed) htmlFile += '</tr>\n';
		// Close the table
		htmlFile += '</table>\n';
	}
	if (++shn > numSheets) break;
}
// Close the file
htmlFile += '</div></body></html>';

// Write htmlFile variable to the disk with newFileName as the name
fs.writeFile(newFileName, htmlFile, (err) => {
	if (err) throw err;
	console.log('\n' +'Your tables have been created in', newFileName);
});
