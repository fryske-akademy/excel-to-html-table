# excel-to-html-table

Improved version of https://github.com/joshboyan/excel-to-html-table.

Convert columns in excel sheet(s) to html5 page with table and embedded search function. Configuarble number of columns/sheets and helpt texts.

## Features

* only process configurable number of columns and sheets
* just plain table cells, no headings
* wrap in html/body/div
* entities for & and <
* deal with empty cells in last column
* search function (configurable help text, default in frisian)
* branche "nederlands" with dutch text and "english" with english text
* zebra stripes
* utf-8 charset

## usage

* clone this repo
* cd into the directory
* install npm
* install nodejs
* npm install xlsx
* review variables in the top of index.js
* node index.js "path to your excel"
